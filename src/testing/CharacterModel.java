/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import mygame.Player;

/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class CharacterModel extends SimpleApplication {

    private static CharacterModel app;
    
    public static void main(String[] args) {
        app = new CharacterModel();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        /*
        ** physics
        */
        RigidBodyControl rigidBodyControl = new RigidBodyControl();
        BulletAppState bulletAppState = new BulletAppState();
        bulletAppState.setDebugEnabled( true );
        app.getStateManager().attach(bulletAppState);
        bulletAppState.getPhysicsSpace().setGravity(new Vector3f(0, -9.81f, 0));
        bulletAppState.getPhysicsSpace().setAccuracy(0.016f);
        
        
        /*
        ** adding the floor
        */
        Box floor = new Box( new Vector3f(0, 0, 0), 20, 0, 20);
        Geometry floorGeom = new Geometry("Floor", floor);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.LightGray);
        floorGeom.setMaterial(mat);
        
        CollisionShape shape = CollisionShapeFactory.createMeshShape(
            floorGeom);
        floorGeom.addControl( new RigidBodyControl(shape, 0));
        bulletAppState.getPhysicsSpace().add( floorGeom );
        
        
        rootNode.attachChild( floorGeom );
        
        
        /*
        ** adding the character
        */
        Player player = new Player(assetManager, rootNode,
                bulletAppState.getPhysicsSpace());
        
        
        /*
        ** adding the character as a vehicle object
        */
//        Vehicle playerVehicle = new Vehicle("Models/FinishedDriverCat.j3o",
//                "Scenes/FlagScene.j3o", assetManager);
//        playerVehicle.attachTo( rootNode, bulletAppState.getPhysicsSpace() );
//        
        
        
    }

    @Override
    public void simpleUpdate(float tpf) {
    
        
    }

    @Override
    public void simpleRender(RenderManager rm) {
       
    }
    
}
