/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 *
 */
public class TrafficControl implements Runnable {
    private volatile static Random random = new Random();
    private volatile boolean isEnabled = true;
    private Node collidables, spawningNode;

    private AssetManager assetManager;
    private PhysicsSpace physicsSpace;
    private Vector3f position;
    private Quaternion rotation;

    private static final int ACCELERATION_FORCE = 50;
    private volatile ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

    // objects to control what vehicle must be detached from the rootNode
    public volatile boolean haveToDetach = false;
    private volatile ArrayList<Vehicle> vehicleToDetach = new ArrayList<Vehicle>();

    // vehicles' models source paths
    private static final String MODEL_FOLDER = "Models/Vehicles/";
    private static final String [] MODEL_PATHS = {
            "BlueishCar.j3o",
            "WhiteCar.j3o",
            "RedCar.j3o"
        };
    private static final Node [] VEHICLE_MODELS = new Node[MODEL_PATHS.length];
    
 
    public TrafficControl(Node collidables, AssetManager assetManager,
            PhysicsSpace physicsSpace, VehicleSpawningPoint spawningPoint) {
        this.physicsSpace = physicsSpace;
        spawningNode = spawningPoint.spawningNode;
        this.collidables = collidables;
        this.assetManager = assetManager;
        
        // loading models into VEHICLE_MODELS using VEHICLE_PATHS' paths
        for (int i = 0; i < VEHICLE_MODELS.length; i++) {
            VEHICLE_MODELS[i] = (Node) assetManager.loadModel(
                    MODEL_FOLDER + MODEL_PATHS[i]); 
        }

        position = spawningPoint.position;
        rotation = spawningPoint.rotation;

        addVehicle();
    }

    @Override
    public void run() {
        Vehicle auxVehicle;
        while (isEnabled) {
            for (int i = 0; i < vehicles.size(); i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                }
                
                auxVehicle = vehicles.get(i);
                if ( auxVehicle.isOnGround(collidables) ) {
                       auxVehicle.getVehicleControl().accelerate(
                               ACCELERATION_FORCE);
                } else {
                    vehicleToDetach.add( auxVehicle ); 
                    haveToDetach = true;
                    vehicles.remove(i);   // esta línea la caga
                    i--;
                }
            }
        }
    }



    public synchronized void addVehicle() {
        Vehicle vehicle = new Vehicle( loadRandomCar().clone(true) );
        vehicle.attachTo( spawningNode, physicsSpace, position, rotation);
        vehicles.add(vehicle);
    }

    public void removeVehicle() {
        Vehicle aux = vehicleToDetach.get(0);
        spawningNode.detachChild( aux.getNode());
        physicsSpace.remove( aux.getVehicleControl());
        vehicleToDetach.remove(0);

        if (vehicleToDetach.isEmpty()) {
            haveToDetach = false;
        }
    }

    
    private Node loadRandomCar () {
        return VEHICLE_MODELS[random.nextInt(VEHICLE_MODELS.length)];
    }

    public void dispose() {
        isEnabled = false;
    }

}
