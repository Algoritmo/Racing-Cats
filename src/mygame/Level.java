/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.asset.MaterialKey;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.util.TangentBinormalGenerator;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;

/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class Level {
    private Node rootNode, levelNode;
    private Spatial level;
    private BulletAppState bulletAppState;
    private AssetManager assetManager;
    private Node [] children;
    
    
    // trees stuff
    private final int TREE_OFFSET = 10;
    private Node [] trees = new Node [4];
    
    // street lights stuff
    private final float POSITION_OFFSET = 48.5f;
    private Node [] streetLights = new Node [4];
    
    // other stuff
    private final String MODEL_PATH = "Models/TestLevel.j3o";
    private final float BLOCK_SIZE = 100;
    
    private final Node [] palets = new Node [2];
    
    
    
    public Level ( Node rootNode, AssetManager assetManager, BulletAppState bulletAppState ) {
        this.rootNode = rootNode;
        this.bulletAppState = bulletAppState;
        this.assetManager = assetManager;
        
        level = assetManager.loadModel( MODEL_PATH );
        levelNode = new Node ();
        
        trees[0] =  (Node) assetManager.loadModel("Models/TestLevel - SingleTree.j3o");
        trees[0] = (Node) trees[0].getChild(0);
        
        
        /*
        ** trees
        */
        Geometry geo = (Geometry) ((Node) trees[0].getChild("Trunk")).getChild(0);
        Mesh mesh = geo.getMesh();
        TangentBinormalGenerator.generate(mesh);
        
        geo = (Geometry) ((Node) trees[0].getChild("Leaves")).getChild(0);
        mesh = geo.getMesh();
        TangentBinormalGenerator.generate(mesh);
        
        
        /*
        ** StreetLights
        */
        streetLights[0] =  (Node) assetManager.loadModel("Models/StreetResources/"
                + "Semaforo.j3o");
        streetLights[0] = (Node) streetLights[0].getChild(0);
        
        geo = (Geometry) ((Node) trees[0].getChild("Trunk")).getChild(0);
        mesh = geo.getMesh();
        TangentBinormalGenerator.generate(mesh);
        
        
        /*
        ** palets
        */
        palets[0] =  (Node) assetManager.loadModel("Models/StreetResources/"
                + "FinishedPalet.j3o");
        palets[0] = (Node) palets[0].getChild(0);
        palets[0] = (Node) palets[0].getChild(0);
        
        geo = (Geometry) palets[0].getChild(0);
        mesh = geo.getMesh();
        TangentBinormalGenerator.generate(mesh);
    }
    
    
    
    public void attachToRoot () {
        levelNode = (Node) level;
        levelNode = (Node) levelNode.getChild("Scene");
        children = new Node[levelNode.getQuantity()];
        Vector3f position;
        
        Node auxNode;
        // attaches all spatials from the .j3o file to the levelNode
        // and adds physics (RigidBodyControl) to them
        CollisionShape shape;
        for ( int i = 0; i < levelNode.getQuantity(); i ++ ) {
            children[i] = new Node ();
            position = new Vector3f( levelNode.getChild(i).getLocalTranslation() );
            children[i] = (Node) levelNode.getChild(i);
            children[i].setLocalTranslation(position);

            shape = CollisionShapeFactory.createMeshShape(
                    children[i].getChild(0) );
            children[i].addControl( new RigidBodyControl( shape, 0) );
            bulletAppState.getPhysicsSpace().add( children[i] );
            levelNode.attachChild( children[i] );
        }
        
        Geometry geo = (Geometry) ((Node) levelNode.getChild("Block.001")).getChild(0);
        Mesh mesh = geo.getMesh();
        TangentBinormalGenerator.generate(mesh);
                        
        /*
        ** adding trees
        */
        addTrees();
                
        /*
        ** adding asphalt material
        */
        addTextures( "Street", "Materials/AsphaltMaterial.j3m", assetManager);
                
        /*
        ** adding stuff
        */
        addStreetLights();
        addPalet();
        
        Fish fish = new Fish(rootNode, assetManager, bulletAppState.getPhysicsSpace());
        
        rootNode.attachChild( levelNode );
    }
    
    
    private void addTrees () {
        for (int i = 1; i < 4; i++) {
            trees[i] = (Node) trees[0].clone();
        }
        
        trees[0].setLocalTranslation( 50 - TREE_OFFSET, 0, 48.5f );
        trees[1].setLocalTranslation( -50 + TREE_OFFSET, 0, - 48.5f);
        trees[2].setLocalTranslation( 48.5f, 0, -50 + TREE_OFFSET);
        trees[3].setLocalTranslation( -48.5f, 0, 50 - TREE_OFFSET);
        
        
        CollisionShape shape;
        Geometry geo;
        Node node;
        
        for (int i = 0; i < 4; i++) {
            shape  = CollisionShapeFactory.createMeshShape(
                    trees[i].getChild("Trunk") );
            trees[i].addControl( new RigidBodyControl( shape, 0) );
            bulletAppState.getPhysicsSpace().add( trees[i] );
            levelNode.attachChild( trees[i] );
        }
    }
    
    
    private void addStreetLights () {
        Quaternion rotation = new Quaternion ();
        rotation.fromAngleAxis( -90 * FastMath.DEG_TO_RAD, Vector3f.UNIT_Y );
        
        for (int i = 1; i < 4; i++) {
            streetLights[i] = (Node) streetLights[0].clone();
        }
        
        streetLights[0].setLocalTranslation( POSITION_OFFSET, 0, POSITION_OFFSET );
        streetLights[1].setLocalTranslation( -POSITION_OFFSET, 0, POSITION_OFFSET);
        streetLights[1].setLocalRotation(rotation);
        
        streetLights[2].setLocalTranslation( POSITION_OFFSET, 0, -POSITION_OFFSET);
        rotation.fromAngleAxis( 90 * FastMath.DEG_TO_RAD, Vector3f.UNIT_Y );
        streetLights[2].setLocalRotation(rotation);
        
        streetLights[3].setLocalTranslation( -POSITION_OFFSET, 0, -POSITION_OFFSET);
        rotation.fromAngleAxis( 180 * FastMath.DEG_TO_RAD, Vector3f.UNIT_Y );
        streetLights[3].setLocalRotation(rotation);
        
        
        CollisionShape shape;
        Geometry geo;
        Node node;
        
        for (int i = 0; i < 4; i++) {
            shape  = CollisionShapeFactory.createMeshShape(
                    streetLights[i].getChild(0) );
            streetLights[i].addControl( new RigidBodyControl( shape, 0) );
            bulletAppState.getPhysicsSpace().add( streetLights[i] );
            levelNode.attachChild( streetLights[i] );
        }
    }
    
    
    private void addPalet () {
        palets[1] = (Node) palets[0].clone();
        
        palets[0].setLocalTranslation(2, .5f, 2);
        palets[1].setLocalTranslation(2.2f, 1, 2);
        
        Geometry geo;
        BoundingBox box;
        BoxCollisionShape shape;        
        
        
        for (int i = 0; i < palets.length; i++) {
            geo = (Geometry) palets[i].getChild(0);
            box = (BoundingBox) geo.getModelBound();
            shape = new BoxCollisionShape( new Vector3f( box.getXExtent(),
                    box.getYExtent(), box.getZExtent()));
            
            palets[i].addControl( new RigidBodyControl( shape, 10) );
            bulletAppState.getPhysicsSpace().add( palets[i] );
            levelNode.attachChild( palets[i] );
        }
    }
    
    
    private void addTextures ( String surfaceType, String materialPath,
            AssetManager assetManager ) {
        Material material = assetManager.loadAsset( new MaterialKey( materialPath ));
        Geometry geo;
        Node node;
        
        for ( int i = 0; i < levelNode.getQuantity(); i++ ) {
            if ( levelNode.getChild(i).getName().contains( surfaceType ) ) {
                node  = (Node) levelNode.getChild(i);
                geo = (Geometry) node.getChild(0);
                Mesh mesh = geo.getMesh();
                TangentBinormalGenerator.generate(mesh);
            }
        }
    }
    
       
 
    public void detach () {
        rootNode.detachChild( levelNode );
    }
    
    public Spatial getSpatial () {
        return level;
    }
    
    public Node getNode () {
        return levelNode;
    }
}
