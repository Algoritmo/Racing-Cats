/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* Lista de bugs a arreglar:
    
*/
package mygame;

import com.jme3.app.state.AbstractAppState;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class GameState extends AbstractAppState implements ActionListener{
    private Node rootNode, levelNode;
    private Camera cam;
    private CameraNode camNode;
    private AssetManager assetManager;
    private InputManager inputManager;
    private Player playerObject;
    private static TrafficControl [] trafficControls;
    private static Vehicle auxVehicle;
    private int counter = 0;
    private final int LIMIT = 100;
    private PhysicsSpace physicsSpace;
    
    
    private String binding = "";
    
    
    //physics stuff
    private RigidBodyControl rigidBodyControl;
    private BulletAppState bulletAppState;
    
          
    
    public void initialize ( Main app ) {
        rootNode = app.getRootNode();
        assetManager = app.getAssetManager();
        inputManager =  app.getInputManager();
        cam = app.getCamera();
        
        bulletAppState = new BulletAppState();
        app.getStateManager().attach(bulletAppState);
        bulletAppState.setDebugEnabled( true );
        bulletAppState.getPhysicsSpace().setGravity( new Vector3f(0, -9.81f, 0) );
        bulletAppState.getPhysicsSpace().setAccuracy( 1f/80f );
//        bulletAppState.getPhysicsSpace().setMaxSubSteps(100);
        physicsSpace = bulletAppState.getPhysicsSpace();
                
        
        // Adding the level
        Level level = new Level ( rootNode, assetManager, bulletAppState );
        level.attachToRoot();
        levelNode = level.getNode();
              
        
        // adding the player
        playerObject = new Player(assetManager, rootNode, physicsSpace);
        
        // adding traffic behavior
//        setTrafficControls();
                

        //adding skymap
        Spatial sky = assetManager.loadModel("Models/Sky.j3o");
        rootNode.attachChild(sky);
        
        setCamera(app);
        setMapping();
        setLighting();
    }
    
    private void setCamera ( Main app ) {
        app.getFlyByCamera().setEnabled(false);
                
        camNode = new CameraNode("CamNode", cam);
        camNode.setLocalTranslation(new Vector3f(0, .75f, -2f));
        playerObject.getPlayerNode().attachChild( camNode );
        camNode.setEnabled(true);
        cam.lookAt( cam.getLocation(), Vector3f.UNIT_Z );
        
//        app.getFlyByCamera().setMoveSpeed(10);
//        cam.setLocation( new Vector3f( 0, 10, 0));
    }
     
    
    @Override
    public void onAction(String binding, boolean isPressed, float tpf) {        
        if (binding.equals("Lefts")) {
            playerObject.turnLeft(isPressed);
        } else if ( binding.equals("Rights") ) {
            playerObject.turnRight(isPressed);
        } else if (binding.equals("Ups")) {
            playerObject.accelerate(isPressed);
        } else if (binding.equals("Downs")) {
            playerObject.brake(isPressed);
        } else if (binding.equals("Space")) {
            playerObject.jump(isPressed, levelNode);
        } else if (binding.equals("Reset")) {
            playerObject.reset(isPressed);
        } else if ( binding.equals("Reverse")) {
            playerObject.reverse( isPressed );
        }
    }
    
    
    private void setMapping () {
        inputManager.addMapping("Lefts", new KeyTrigger(KeyInput.KEY_A ));
        inputManager.addMapping("Rights", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Ups", new KeyTrigger(KeyInput.KEY_L));
        inputManager.addMapping("Downs", new KeyTrigger(KeyInput.KEY_K));
        inputManager.addMapping("Space", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Reset", new KeyTrigger(KeyInput.KEY_B));
        inputManager.addMapping("Reverse", new KeyTrigger(KeyInput.KEY_S));
        
        inputManager.addListener(this, "Lefts");
        inputManager.addListener(this, "Rights");
        inputManager.addListener(this, "Ups");
        inputManager.addListener(this, "Downs");
        inputManager.addListener(this, "Space");
        inputManager.addListener(this, "Reset");
        inputManager.addListener(this, "Reverse");
    }
    
    
    private void setTrafficControls () {
        trafficControls = new TrafficControl[8];
        
        // EW cars
        Spatial spatial = assetManager.loadModel("Scenes/FlagScene.j3o");
        Node auxNode  = (Node) spatial;
        Vector3f position = auxNode.getChild("Flag1").getLocalTranslation();
        
        VehicleSpawningPoint spawningPoint = new VehicleSpawningPoint(
                position, VehicleSpawningPoint.EAST_D, rootNode);
        TrafficControl auxTrafficControl = new TrafficControl( levelNode, assetManager,
                physicsSpace, spawningPoint );
        Thread trafficThread = new Thread(auxTrafficControl);
        trafficThread.start();
        
        trafficControls[0] = auxTrafficControl;
        
                
        // WE cars
        position = auxNode.getChild("Flag2").getLocalTranslation();
        
        spawningPoint = new VehicleSpawningPoint(
                position, VehicleSpawningPoint.WEST_D, rootNode);
        auxTrafficControl = new TrafficControl( levelNode, assetManager,
                physicsSpace, spawningPoint );
        Thread trafficThread2 = new Thread(auxTrafficControl);
        trafficThread2.start();
        
        trafficControls[1] = auxTrafficControl;
        
        
        // EW cars
        position = auxNode.getChild("Flag3").getLocalTranslation();
        
        spawningPoint = new VehicleSpawningPoint(
                position, VehicleSpawningPoint.EAST_D, rootNode);
        auxTrafficControl = new TrafficControl( levelNode, assetManager,
                physicsSpace, spawningPoint );
        Thread trafficThread3 = new Thread(auxTrafficControl);
        trafficThread3.start();
        
        trafficControls[2] = auxTrafficControl;
        
        
        // WE cars
        position = auxNode.getChild("Flag4").getLocalTranslation();
        
        spawningPoint = new VehicleSpawningPoint(
                position, VehicleSpawningPoint.WEST_D, rootNode);
        auxTrafficControl = new TrafficControl( levelNode, assetManager,
                physicsSpace, spawningPoint );
        Thread trafficThread4 = new Thread(auxTrafficControl);
        trafficThread4.start();
        
        trafficControls[3] = auxTrafficControl;
        
        
        // SN car
        position = auxNode.getChild("Flag5").getLocalTranslation();
        
        spawningPoint = new VehicleSpawningPoint(
                position, VehicleSpawningPoint.NORTH_D, rootNode);
        auxTrafficControl = new TrafficControl( levelNode, assetManager,
                physicsSpace, spawningPoint );
        Thread trafficThread5 = new Thread(auxTrafficControl);
        trafficThread5.start();
        
        trafficControls[4] = auxTrafficControl;
        
        // SN car
        position = auxNode.getChild("Flag6").getLocalTranslation();
        
        spawningPoint = new VehicleSpawningPoint(
                position, VehicleSpawningPoint.NORTH_D, rootNode);
        auxTrafficControl = new TrafficControl( levelNode, assetManager,
                physicsSpace, spawningPoint );
        Thread trafficThread6 = new Thread(auxTrafficControl);
        trafficThread6.start();
        
        trafficControls[5] = auxTrafficControl;
        
        
        // NS car
        position = auxNode.getChild("Flag7").getLocalTranslation();
        
        spawningPoint = new VehicleSpawningPoint(
                position, VehicleSpawningPoint.SOUTH_D, rootNode);
        auxTrafficControl = new TrafficControl( levelNode, assetManager,
                physicsSpace, spawningPoint );
        Thread trafficThread7 = new Thread(auxTrafficControl);
        trafficThread7.start();
        
        trafficControls[6] = auxTrafficControl;
        
        
        // NS car
        position = auxNode.getChild("Flag8").getLocalTranslation();
        
        spawningPoint = new VehicleSpawningPoint(
                position, VehicleSpawningPoint.SOUTH_D, rootNode);
        auxTrafficControl = new TrafficControl( levelNode, assetManager,
                physicsSpace, spawningPoint );
        Thread trafficThread8 = new Thread(auxTrafficControl);
        trafficThread8.start();
        
        trafficControls[7] = auxTrafficControl;
    }
    
    
    private void setLighting () {
        /**
         * A white ambient light source.
         */
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.LightGray);
        rootNode.addLight(ambient);

        /**
         * A white, directional light source
         */
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection((new Vector3f(-0.5f, -0.5f, -0.5f)).normalizeLocal());
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);
    }
    
    
    @Override
    public void update ( float tpf ) {
//        counter ++;
//        
//        if (counter == LIMIT) {
//            for (int i = 0; i < trafficControls.length; i++) {
//                trafficControls[i].addVehicle();
//            }
//            
//            counter = 0;
//        } else if ( counter % 30 == 0 ) {
//            for (int i = 0; i < trafficControls.length; i++) {
//                if (trafficControls[i].haveToDetach) {
//                    trafficControls[i].removeVehicle();
//                }
//            }
//        }
    }
    
    
    @Override
    public void cleanup () {
//        for ( int i = 0; i < trafficControls.length; i++ ) {
//            trafficControls[i].dispose();
//        }
    }
}
