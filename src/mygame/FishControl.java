/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.GhostControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class FishControl extends AbstractControl {
    public boolean detach = false;
    private GhostControl control;
    private Node node = new Node ();
    private PhysicsSpace physicsSpace;

    public FishControl ( Node node, PhysicsSpace physicsSpace) {
        this.physicsSpace = physicsSpace;
        this.node = node;
        CollisionShape shape = CollisionShapeFactory.createMeshShape(node);
        control = new GhostControl( shape );
        node.addControl(control);
        physicsSpace.add( control );
    }
    
    @Override
    protected void controlUpdate(float tpf) {
        if ( !control.getOverlappingObjects().isEmpty() ) {
            node.removeControl( control );
            spatial.removeControl( this );
            physicsSpace.remove( control );
            node.removeFromParent();
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }
}
