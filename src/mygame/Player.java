/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CompoundCollisionShape;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.util.TangentBinormalGenerator;

/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class Player {
    private Node player;
    private VehicleControl vehicleControl;
    
    
    // values to set up the vehicle control
    private final float ACCELERATION_FORCE = 30.0f, BRAKE_FORCE = 2.0f,
            MAX_SPEED = 60.0f, STEERING = .04f, WEIGHT = 40;
    private float steeringValue = 0, accelerationValue = 0, brakeValue = 0,
            currentSpeed = 0;
    private final Vector3f jumpForce = new Vector3f(0, 200, 0);
    
    
    
    public Player ( AssetManager assetManager, Node rootNode,
            PhysicsSpace physicsSpace ) {
        Quaternion rotation = new Quaternion ();
        rotation.fromAngleAxis(180*FastMath.DEG_TO_RAD, Vector3f.UNIT_Y);
        
        // auxiliar scene
        Spatial assembledCar = assetManager.loadModel(
                "Models/FinishedDriverCat.j3o");
        Node assembledNode = ( Node ) assembledCar;
        assembledNode.attachChild(assembledCar);
        Vector3f chasisPosition = assembledNode.getChild("Cat").
                getLocalTranslation();
        
        
       
        //create a compound shape and attach the BoxCollisionShape for the car body at
        // 0,1,0
        //this shifts the effective center of mass of the BoxCollisionShape to 0,-1,0
        CompoundCollisionShape compoundShape = new CompoundCollisionShape();
        Node chasisNode = (Node) assembledNode.getChild("Cat");
        Geometry kitten = (Geometry) chasisNode.getChild("Cat1");
        Geometry chassis = (Geometry) chasisNode.getChild("Cat2");
        
        
        
        BoundingBox kittenBox = (BoundingBox) kitten.getModelBound();
        BoundingBox chassisBox = (BoundingBox) chassis.getModelBound();

        Mesh mesh = chassis.getMesh();
        TangentBinormalGenerator.generate(mesh);
        
        
        //Create a hull collision shape for the chassis
        BoxCollisionShape carHull = new BoxCollisionShape(
                new Vector3f(chassisBox.getXExtent(),
                        kittenBox.getYExtent(),
                        chassisBox.getZExtent()));
        compoundShape.addChildShape(carHull, new Vector3f( chasisPosition.x,
                kittenBox.getYExtent() + (kittenBox.getYExtent() / 2) - .05f,
                chasisPosition.z) );
        
               
        //create vehicle node
        player = new Node("vehicleNode");
        
        
        vehicleControl = new VehicleControl( compoundShape, WEIGHT);
        player.addControl(vehicleControl);
        player.attachChild(chasisNode);
        
        

        //setting suspension values for wheels, this can be a bit tricky
        float stiffness = 150.0f;
        float compValue = .3f; //(should be lower than damp)
        float dampValue = .4f;
        vehicleControl.setSuspensionCompression(compValue * 2.0f * FastMath.sqrt(stiffness));
        vehicleControl.setSuspensionDamping(dampValue * 2.0f * FastMath.sqrt(stiffness));
        vehicleControl.setSuspensionStiffness(stiffness);
        vehicleControl.setMaxSuspensionForce(100000.0f);

        
        
        /*
        ** Create four wheels and add them at their locations
        */
        Vector3f wheelDirection = new Vector3f(0, -1, 0); // was 0, -1, 0
        Vector3f wheelAxle = new Vector3f(-1, 0, 0); // was -1, 0, 0
        
        float radius;
        float restLength;
        Vector3f wheelPosition;
        Vector3f offSet;

        
        
        
        // rueda izquierda delantera
        Node ridNode = new Node();
        wheelPosition = assembledNode.getChild("RID").getWorldTranslation();
        Spatial ridSpatial = assembledNode.getChild("RID1");
        
        ridNode.attachChild(ridSpatial);
        
        Geometry ridGeom = (Geometry) ridSpatial;
        BoundingBox box = (BoundingBox) ridGeom.getModelBound();
        radius = box.getYExtent();
        restLength = box.getXExtent();
        offSet = new Vector3f(wheelPosition.x, wheelPosition.y , wheelPosition.z);
        
        ridNode.setLocalTranslation(offSet);
        
        vehicleControl.addWheel(ridNode, offSet, wheelDirection, wheelAxle, restLength,
                radius, true);
        
       
        
        // rueda derecha delantera
        Node rddNode = new Node();
        wheelPosition = assembledNode.getChild("RDD").getLocalTranslation();
        Spatial rddSpatial = assembledNode.getChild("RDD1");
         
        rddNode.attachChild(rddSpatial);
        rddNode.setLocalTranslation(offSet);
        
        Geometry rddGeom = (Geometry) rddSpatial;
        box = (BoundingBox) rddGeom.getModelBound();
        radius = box.getYExtent();
        restLength = box.getXExtent();
        offSet = new Vector3f(wheelPosition.x, wheelPosition.y, wheelPosition.z);
        vehicleControl.addWheel(rddNode, offSet, wheelDirection, wheelAxle, restLength,
                radius, true);
        
        
        // rueda izquierda trasera
        Node ritNode = new Node();
        wheelPosition = assembledNode.getChild("RIT").getLocalTranslation();
        Spatial ritSpatial = assembledNode.getChild("RIT1");
        
        ritNode.attachChild(ritSpatial);
        ritNode.setLocalTranslation(offSet);
        
        Geometry ritGeom = (Geometry) ritSpatial;
        box = (BoundingBox) ritGeom.getModelBound();
        radius = box.getYExtent();
        restLength = box.getXExtent();
        offSet = new Vector3f(wheelPosition.x, wheelPosition.y, wheelPosition.z);
        vehicleControl.addWheel(ritNode, offSet, wheelDirection, wheelAxle, restLength,
                radius, false);
        
        
        // rueda derecha trasera
        Node rdtNode = new Node();
        wheelPosition = assembledNode.getChild("RDT").getLocalTranslation();
        Spatial rdtSpatial = assembledNode.getChild("RDT1");
        
        rdtNode.attachChild(rdtSpatial);
        rdtNode.setLocalTranslation(offSet);
        
        Geometry rdtGeom = (Geometry) rdtSpatial;
        radius = box.getYExtent();
        restLength = box.getXExtent();
        offSet = new Vector3f(wheelPosition.x, wheelPosition.y, wheelPosition.z);
        vehicleControl.addWheel(rdtNode, offSet, wheelDirection, wheelAxle, restLength,
                radius, false);
        
                
        player.attachChild(ridNode);
        player.attachChild(rddNode);
        player.attachChild(ritNode);
        player.attachChild(rdtNode);
        rootNode.attachChild(player);
        
        System.out.println( player.getChild("Cat").getWorldTranslation() );
        
        
        physicsSpace.add(player);
        vehicleControl.setPhysicsLocation( new Vector3f ( 0, .5f, 0));
//        vehicleControl.setPhysicsRotation( rotation );
    }
    
    public Node getPlayerNode () {
        return player;
    }
    
    public VehicleControl getVehicleControl () {
        return vehicleControl;
    }
    
    
    public boolean isOnGround ( Node collidableNode ) {
        boolean isOnGround = true;
        
        CollisionResults results = new CollisionResults();
        Vector3f wheelLocation = new Vector3f();
        
        vehicleControl.getWheel(0).getWheelWorldLocation( wheelLocation );
        Ray ray = new Ray( wheelLocation,
                vehicleControl.getWheel(0).getDirection());

        collidableNode.collideWith(ray, results);
        
                
        if (!(results.size() > 0)) {
            isOnGround = false;
        } else if (results.size() > 0) {
            CollisionResult closest = results.getClosestCollision();

            if (closest.getDistance() > .5f) {
                isOnGround = false;
            }
        }
            
        return isOnGround;
    }

    /*
    ** driving instructions
    */
    public void turnLeft ( boolean isPressed ) {
        if (isPressed) {
            steeringValue += STEERING;
        } else {
            steeringValue -= STEERING;
        }
        vehicleControl.steer(steeringValue);
    }
    
    public void turnRight ( boolean isPressed ) {
        if (isPressed) {
            steeringValue -= STEERING;
        } else {
            steeringValue += STEERING;
        }
        vehicleControl.steer(steeringValue);
    }
    
    public void accelerate ( boolean isPressed ) {
//        if (isPressed) {
//            if ( accelerationValue < 100 )
//                accelerationValue += ACCELERATION_FORCE;
//        } else {
//            if ( accelerationValue > 0 )
//                accelerationValue -= ACCELERATION_FORCE;
//        }
//        if ( accelerationValue < 100    &&   accelerationValue >= 0 )
//            vehicleControl.accelerate(accelerationValue);
        
        if (isPressed ) {
            currentSpeed = vehicleControl.getCurrentVehicleSpeedKmHour();
            if (accelerationValue < 100     &&  currentSpeed < MAX_SPEED ) {
                accelerationValue += ACCELERATION_FORCE;
            }
        } else {
            if (accelerationValue > 0) {
                accelerationValue -= ACCELERATION_FORCE;
            }
        }
        if (accelerationValue < 100 && accelerationValue >= 0) {
            vehicleControl.accelerate(accelerationValue);
        }
        

    }
    
    public void reverse ( boolean isPressed ) {
        if (isPressed) {
            accelerationValue -= ACCELERATION_FORCE;
            
        } else {
            if ( accelerationValue < 0 )
                accelerationValue += ACCELERATION_FORCE;
        }
        vehicleControl.accelerate(accelerationValue);
    }
    
    public void brake ( boolean isPressed ) {
        if (isPressed) {
            vehicleControl.brake(BRAKE_FORCE);
            accelerationValue = 0;
        } else {
            vehicleControl.brake(.15f);
        }
    }
    
    public void jump ( boolean isPressed, Node collidables) {
        if ( isPressed && isOnGround( collidables )) {
            vehicleControl.applyImpulse(jumpForce, Vector3f.ZERO);
        }
    }
    
    public void reset ( boolean isPressed ) {
        if (isPressed) {
            vehicleControl.setPhysicsLocation(Vector3f.ZERO);
            vehicleControl.setPhysicsRotation(new Matrix3f());
            vehicleControl.setLinearVelocity(Vector3f.ZERO);
            vehicleControl.setAngularVelocity(Vector3f.ZERO);
            vehicleControl.resetSuspension();
        } else {
        }
    }
}
