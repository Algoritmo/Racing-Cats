/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;

/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class Fish {
    private String FISH_MODEL = "Models/StreetResources/Fish.j3o";
    
    public Fish ( Node parentNode, AssetManager assetManager,
            PhysicsSpace physicsSpace ) {
        Node fish = (Node) assetManager.loadModel( FISH_MODEL );
        fish = (Node) fish.getChild(0);
        
        Geometry geo = (Geometry) ((Node) fish.getChild(0)).getChild(0);
        
        parentNode.attachChild( fish );
        fish.setLocalTranslation( new Vector3f ( 0, 0, -3) );
        
        fish.addControl( new FishControl( fish, physicsSpace ));
        physicsSpace.add( fish );
    }
}
