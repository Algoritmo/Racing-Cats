package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.renderer.RenderManager;

/**
 * This is the Main Class of your Game. You should only do initialization here.
 * Move your Logic into AppStates or Controls
 * Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class Main extends SimpleApplication {
    private static Main app;
    private static GameState gameState;
    
    public static void main(String[] args) {
        app = new Main();
        gameState = new GameState();
        app.start();
    }

    @Override
    public void simpleInitApp() {
//        setDisplayFps(false);
//        setDisplayStatView(false);

        stateManager.attach(gameState);
        gameState.initialize(app);
        
        
        /** Write text on the screen (HUD) */
        guiNode.detachAllChildren();
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText helloText = new BitmapText(guiFont, false);
        helloText.setSize(guiFont.getCharSet().getRenderedSize());
        helloText.setText("Hello World");
        helloText.setLocalTranslation(300, helloText.getLineHeight(), 0);
        guiNode.attachChild(helloText);
 
    }

    @Override
    public void simpleUpdate(float tpf) {
    
        
    }

    @Override
    public void simpleRender(RenderManager rm) {
       
    }
}
