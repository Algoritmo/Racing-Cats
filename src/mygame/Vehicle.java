/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CompoundCollisionShape;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;


/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class Vehicle {
    private volatile Node vehicleNode, auxNode, rootNode;
    private VehicleControl vehicleControl;
    private volatile PhysicsSpace physicsSpace;
    private Vector3f position;
    
    // values to set up the vehicle control
    private final float ACCELERATION_FORCE = 10.0f, BRAKE_FORCE = 1.0f,
            WHEEL_COLLISION_OFFSET = 1;            ;
    private float wheelCollisionDistance;
    
    
    public Vehicle ( Node vehicleModel ) {        
        // assembled car spatial
        Node assembledNode = vehicleModel;
        assembledNode.attachChild( assembledNode );     
        
       
        //create a compound shape and attach the BoxCollisionShape for the car 
        //body at 0,1,0 this shifts the effective center of mass of the
        //BoxCollisionShape to 0,-1,0
        CompoundCollisionShape compoundShape = new CompoundCollisionShape();
        Node chasisNode = (Node) assembledNode.getChild("Chassis");
        Geometry chasis = (Geometry) chasisNode.getChild("Chassis1");
      
        BoundingBox box = (BoundingBox) chasis.getModelBound();

        //Create a hull collision shape for the chassis
        BoxCollisionShape carHull = new BoxCollisionShape(
                new Vector3f(box.getXExtent() - .005f, box.getYExtent(), box.getZExtent()));
        compoundShape.addChildShape(carHull, new Vector3f(0, 1.2f, 0));
        
               
        //create vehicle node and vehicleControl
        vehicleNode = new Node("vehicleNode");
        
        vehicleControl = new VehicleControl( compoundShape, 20);
        vehicleNode.addControl(vehicleControl);
        vehicleNode.attachChild(chasisNode);
        
        
        //setting suspension values for wheels, this can be a bit tricky
        float stiffness = 120.0f;//200=f1 car
        float compValue = .3f;   //(should be lower than damp)
        float dampValue = .4f;
        vehicleControl.setSuspensionCompression(
                compValue * 2.0f * FastMath.sqrt(stiffness));
        vehicleControl.setSuspensionDamping(
                dampValue * 2.0f * FastMath.sqrt(stiffness));
        vehicleControl.setSuspensionStiffness(stiffness);
        vehicleControl.setMaxSuspensionForce(10000.0f);

        
        
        /*
        ** Create four wheels and add them at their locations
        */
        Vector3f wheelDirection = new Vector3f(0, -1, 0); // was 0, -1, 0
        Vector3f wheelAxle = new Vector3f(-1, 0, 0); // was -1, 0, 0
        
        float radius;
        float restLength;
        Vector3f wheelPosition;
        Vector3f offSet;
        
        
        // rueda izquierda delantera
        Node ridNode = new Node();
        wheelPosition = assembledNode.getChild("RID").getWorldTranslation();
        Spatial ridSpatial = assembledNode.getChild("RID1");
        
        ridNode.attachChild(ridSpatial);
        
        Geometry ridGeom = (Geometry) ridSpatial;
        box = (BoundingBox) ridGeom.getModelBound();
        radius = box.getYExtent();
        restLength = box.getXExtent();
        offSet = new Vector3f(wheelPosition.x, wheelPosition.y , wheelPosition.z);
        
        ridNode.setLocalTranslation(offSet);
        
        vehicleControl.addWheel(ridNode, offSet, wheelDirection, wheelAxle, restLength,
                radius, true);
        
       
        
        // rueda derecha delantera
        Node rddNode = new Node();
        wheelPosition = assembledNode.getChild("RDD").getLocalTranslation();
        Spatial rddSpatial = assembledNode.getChild("RDD1");
         
        rddNode.attachChild(rddSpatial);
        rddNode.setLocalTranslation(offSet);
        
        Geometry rddGeom = (Geometry) rddSpatial;
        box = (BoundingBox) rddGeom.getModelBound();
        radius = box.getYExtent();
        restLength = box.getXExtent();
        offSet = new Vector3f(wheelPosition.x, wheelPosition.y, wheelPosition.z);
        vehicleControl.addWheel(rddNode, offSet, wheelDirection, wheelAxle, restLength,
                radius, true);
        
        
        // rueda izquierda trasera
        Node ritNode = new Node();
        wheelPosition = assembledNode.getChild("RIT").getLocalTranslation();
        Spatial ritSpatial = assembledNode.getChild("RIT1");
        
        ritNode.attachChild(ritSpatial);
        ritNode.setLocalTranslation(offSet);
        
        Geometry ritGeom = (Geometry) ritSpatial;
        box = (BoundingBox) ritGeom.getModelBound();
        radius = box.getYExtent();
        restLength = box.getXExtent();
        offSet = new Vector3f(wheelPosition.x, wheelPosition.y, wheelPosition.z);
        vehicleControl.addWheel(ritNode, offSet, wheelDirection, wheelAxle, restLength,
                radius, false);
        
        
        // rueda derecha trasera
        Node rdtNode = new Node();
        wheelPosition = assembledNode.getChild("RDT").getLocalTranslation();
        Spatial rdtSpatial = assembledNode.getChild("RDT1");
        
        rdtNode.attachChild(rdtSpatial);
        rdtNode.setLocalTranslation(offSet);
        
        Geometry rdtGeom = (Geometry) rdtSpatial;
        radius = box.getYExtent();
        restLength = box.getXExtent();
        offSet = new Vector3f(wheelPosition.x, wheelPosition.y, wheelPosition.z);
        vehicleControl.addWheel(rdtNode, offSet, wheelDirection, wheelAxle, restLength,
                radius, false);
        
        vehicleNode.attachChild(ridNode);
        vehicleNode.attachChild(rddNode);
        vehicleNode.attachChild(ritNode);
        vehicleNode.attachChild(rdtNode);
        
        wheelCollisionDistance = radius + WHEEL_COLLISION_OFFSET;
    }
    
    
    public void attachTo ( Node rootNode, PhysicsSpace physicsSpace, Vector3f position,
            Quaternion rotation ) {
        this.rootNode = rootNode;
        
        physicsSpace.add(vehicleNode);
        vehicleControl.setPhysicsLocation(position);
        vehicleControl.setPhysicsRotation(rotation);
        rootNode.attachChild(vehicleNode);
    }
       
    
    public VehicleControl getVehicleControl() {
        return vehicleControl;
    }
    
    
    public boolean isOnGround ( Node collidableNode ) {
        boolean isOnGround = true;
        
        CollisionResults results = new CollisionResults();
        Vector3f wheelLocation = new Vector3f();
        vehicleControl.getWheel(0).getWheelWorldLocation( wheelLocation );
        Ray ray = new Ray( wheelLocation,
                vehicleControl.getWheel(0).getDirection());

        collidableNode.collideWith(ray, results);
        
                
        if (!(results.size() > 0)) {
            isOnGround = false;
        } else if (results.size() > 0) {
            CollisionResult closest = results.getClosestCollision();
            
            if ( closest.getDistance() > wheelCollisionDistance ) {
                isOnGround = false;
            }
        }
            
        return isOnGround;
    }
    
    
    public Node getNode () {
        return vehicleNode;
    }
}
