/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class VehicleSpawningPoint {
    public Vector3f position, direction;
    public Quaternion rotation = new Quaternion();
    public Node spawningNode;

    
    /* Class constants: directions  */
    public static final Vector3f NORTH_D = new Vector3f(0, 0, -1);
    public static final Vector3f SOUTH_D = new Vector3f(0, 0, 1);
    public static final Vector3f EAST_D = new Vector3f(1, 0, 0);
    public static final Vector3f WEST_D = new Vector3f(-1, 0, 0);

    
    /* Class constants: rotation ( to make models face the specified direction ) */
    public static final float NORTH_R = 0, SOUTH_R = 180, EAST_R = 90, WEST_R = -90;

    
    public VehicleSpawningPoint(Vector3f position, Vector3f direction,
            Node spawningNode) {
        this.spawningNode = spawningNode;
        this.position = position;
        this.direction = direction;

        float degrees = 0;
        if (direction == SOUTH_D) {
            degrees = SOUTH_R;
        } else if (direction == EAST_D) {
            degrees = EAST_R;
        } else if (direction == WEST_D) {
            degrees = WEST_R;
        }

        rotation.fromAngleAxis( degrees * FastMath.DEG_TO_RAD, Vector3f.UNIT_Y );
    }
}
