/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trackDesign.testing;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.input.ChaseCamera;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.util.TangentBinormalGenerator;
import mygame.Player;

/**
 *
 * @author Iris. Find my games in: https://lapsicopata.itch.io/
 */
public class Main extends SimpleApplication implements ActionListener {
    private static Main app;
    
    private final String TRACK = "Models/Tracks/Street modules.j3o";
    private static CameraNode camNode;
    
    /*
    ** physics stuff
    */
    private static BulletAppState bulletAppState = new BulletAppState();
    private Player playerObject;
        
    
    
    public static void main(String[] args) {
        app = new Main();
        app.start();
    }
    
    @Override
    public void simpleInitApp() {
        stateManager.attach(bulletAppState);
        
        bulletAppState.getPhysicsSpace().setGravity(new Vector3f ( 0, -9.8f, 0) );
        bulletAppState.getPhysicsSpace().setAccuracy( 0.016f );
//        bulletAppState.setDebugEnabled(true);
        
        
        addTrack();
        addPlayer();
        viewPort.setBackgroundColor(ColorRGBA.Cyan);
        
        app.getFlyByCamera().setEnabled(false);
        setCamera();
        setMapping();
        
        /**
         * A white ambient light source.
         */
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.White);
        rootNode.addLight(ambient);
    }
    
    private void setCamera () {
        flyCam.setEnabled(false);
          
        ChaseCamera chaseCam = new ChaseCamera(cam, playerObject.getPlayerNode(),
                inputManager);
        chaseCam.setSmoothMotion(true);
        chaseCam.setChasingSensitivity( 100 );
        chaseCam.setDefaultDistance( 1.7f );
        chaseCam.setMinDistance( 1.5f );
        chaseCam.setMaxDistance( 2f );
        
        Node targetNode = new Node ();
        targetNode.setLocalTranslation( new Vector3f(0, 1, 0));
        playerObject.getPlayerNode().attachChild( targetNode );
        chaseCam.setSpatial( targetNode );
        
        chaseCam.setDefaultVerticalRotation( 5 * FastMath.DEG_TO_RAD );
        
        cam.setLocation( new Vector3f(0, .75f, -2f));
    }
    
    public void update(float tpf) {
    }
        
    
    public void cleanup() {
    }
    
    
    private void addTrack () {
        Spatial level = assetManager.loadModel( TRACK );
        Node levelNode = (Node) level;
        levelNode = (Node) levelNode.getChild("Scene");
        Node [] children = new Node[levelNode.getQuantity()];
        Vector3f position;
        
        Node auxNode;
        // attaches all spatials from the .j3o file to the levelNode
        // and adds physics (RigidBodyControl) to them
        CollisionShape shape;
        for ( int i = 0; i < levelNode.getQuantity(); i ++ ) {
            children[i] = new Node ();
            position = new Vector3f( levelNode.getChild(i).getLocalTranslation() );
            children[i] = (Node) levelNode.getChild(i);
            children[i].setLocalTranslation(position);

            // generates tangent binormals
            Geometry geo;
            Mesh mesh;
            for ( int j = 0; j < children[i].getQuantity(); j++ ) {
                geo = (Geometry) children[i].getChild(j);
                mesh = geo.getMesh();
                TangentBinormalGenerator.generate(mesh);
            }
            
            shape = CollisionShapeFactory.createMeshShape(
                    children[i] );
            children[i].addControl( new RigidBodyControl( shape, 0) );
            bulletAppState.getPhysicsSpace().add( children[i] );
            levelNode.attachChild( children[i] );
        }
        
        rootNode.attachChild( levelNode );
    }
    
    
    private void addPlayer () {
        playerObject = new Player(assetManager, rootNode,
                bulletAppState.getPhysicsSpace());
        
        Quaternion rotation = new Quaternion ();
        rotation.fromAngleAxis( 180 * FastMath.DEG_TO_RAD, Vector3f.UNIT_Y);
        playerObject.getVehicleControl().setPhysicsRotation(rotation);
    }

    
    @Override
    public void onAction(String binding, boolean isPressed, float tpf) {
        if (binding.equals("Lefts")) {
            playerObject.turnLeft(isPressed);
        } else if ( binding.equals("Rights") ) {
            playerObject.turnRight(isPressed);
        } else if (binding.equals("Ups")) {
            playerObject.accelerate(isPressed);
        } else if (binding.equals("Downs")) {
            playerObject.brake(isPressed);
        } else if (binding.equals("Space")) {
            playerObject.jump(isPressed, rootNode);
        } else if (binding.equals("Reset")) {
            playerObject.reset(isPressed);
        } else if ( binding.equals("Reverse")) {
            playerObject.reverse( isPressed );
        }
    }
    
    
    private void setMapping () {
        inputManager.addMapping("Lefts", new KeyTrigger(KeyInput.KEY_A ));
        inputManager.addMapping("Rights", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Ups", new KeyTrigger(KeyInput.KEY_L));
        inputManager.addMapping("Downs", new KeyTrigger(KeyInput.KEY_K));
        inputManager.addMapping("Space", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Reset", new KeyTrigger(KeyInput.KEY_B));
        inputManager.addMapping("Reverse", new KeyTrigger(KeyInput.KEY_S));
        
        inputManager.addListener(this, "Lefts");
        inputManager.addListener(this, "Rights");
        inputManager.addListener(this, "Ups");
        inputManager.addListener(this, "Downs");
        inputManager.addListener(this, "Space");
        inputManager.addListener(this, "Reset");
        inputManager.addListener(this, "Reverse");
    }
}
